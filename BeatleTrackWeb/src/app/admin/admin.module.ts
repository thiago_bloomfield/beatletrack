import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {
  FeatureGroupManagerCreateModalComponent,
} from './feature-group-manager/feature-group-manager-create-modal/feature-group-manager-create-modal.component';
import { FeatureGroupManagerComponent } from './feature-group-manager/feature-group-manager.component';
import { FeatureGroupManagerService } from './feature-group-manager/feature-group-manager.service';
import { FeatureManagerComponent } from './feature-manager/feature-manager.component';
import { FeatureManagerService } from './feature-manager/feature-manager.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    DashboardComponent,
    FeatureGroupManagerComponent,
    FeatureManagerComponent,
    FeatureGroupManagerCreateModalComponent
  ],
  imports: [
    AdminRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatDividerModule
  ],
  providers: [
    FeatureGroupManagerService,
    FeatureManagerService
  ],
  entryComponents: [
    FeatureGroupManagerCreateModalComponent
  ]
})
export class AdminModule { }
