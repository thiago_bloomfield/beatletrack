import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { auth } from 'firebase';
import { NgxSpinnerService } from 'ngx-spinner';
import { of, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';

import { UserModel } from './../../core/models/user.model';
import { AuthenticationService } from './../../core/services/auth/authentication.service';
import { UserService } from './../../core/services/user/user.service';
import { ErrorService } from './../../shared/services/error.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  user: UserModel = null;
  form: FormGroup;

  private unsubscribeAll = new Subject();

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      userEmail: this.formBuilder.control('', [Validators.email]),
      userPassword: this.formBuilder.control('', [Validators.minLength(6)])
   });
  }

  signIn(): void {
    const email = this.form.get('userEmail').value;
    const userPassword = this.form.get('userPassword').value;
    this.spinner.show();
    this.authenticationService
      .signInWithEmailAndPassword(email, userPassword)
      .pipe(
        // tap((userInfo: auth.UserCredential) => {
        //   this.user = {
        //     displayName: userInfo.user.displayName,
        //     email: userInfo.user.email,
        //     phoneNumber: userInfo.user.phoneNumber,
        //     photoURL: userInfo.user.photoURL.toString(),
        //     uid: userInfo.user.uid
        //   };
        // }),
        switchMap((userInfo: auth.UserCredential) => this.userService.searchUserByEmail(userInfo.user)),
        catchError((error: firebase.FirebaseError) => {
          this.spinner.hide();
          this.errorService.displayError(error);
          return of(null);
        }),
        takeUntil(this.unsubscribeAll)
      )
      .subscribe((user: UserModel) => {
        console.log(user);
        this.spinner.hide();
      })
  }
}