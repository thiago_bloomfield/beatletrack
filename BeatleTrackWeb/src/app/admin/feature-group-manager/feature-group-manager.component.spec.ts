import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureGroupManagerComponent } from './feature-group-manager.component';

describe('FeatureGroupManagerComponent', () => {
  let component: FeatureGroupManagerComponent;
  let fixture: ComponentFixture<FeatureGroupManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureGroupManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureGroupManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
