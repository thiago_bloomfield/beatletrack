import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject, throwError } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { FeatureGroupModel } from './../../../core/models/feature-group.model';
import { ErrorService } from './../../../shared/services/error.service';
import { FeatureGroupManagerService } from './../feature-group-manager.service';

@Component({
  selector: 'app-feature-group-manager-create-modal',
  templateUrl: './feature-group-manager-create-modal.component.html',
  styleUrls: ['./feature-group-manager-create-modal.component.scss']
})
export class FeatureGroupManagerCreateModalComponent implements OnInit, OnDestroy {

  form: FormGroup;

  private destroy$ = new Subject();

  constructor(
    public dialogRef: MatDialogRef<FeatureGroupManagerCreateModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private featureGroupManagerService: FeatureGroupManagerService,
    private spinner: NgxSpinnerService,
    private snackBar: MatSnackBar,
    private errorService: ErrorService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initForm(): void {
    if (!!this.data && !!this.data.name) {
      this.form = this.formBuilder.group({
        name: this.formBuilder.control(this.data.name, [Validators.min(1)]),
        description: this.formBuilder.control(this.data.description, [Validators.min(1)])
      });
      this.form.addControl('fgid', this.formBuilder.control(this.data.fgid));
      this.form.addControl('functionalities', this.formBuilder.control(this.data.functionalities));
    } else {
      this.form = this.formBuilder.group({
        name: this.formBuilder.control('', [Validators.min(1)]),
        description: this.formBuilder.control('', [Validators.min(1)]),
      });
      this.form.addControl('fgid', this.formBuilder.control(this.featureGroupManagerService.getNewFeatureGroupId()));
      this.form.addControl('functionalities', this.formBuilder.control([]));
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onAdicionar(): void {
    if (this.form.invalid) {
      return;
    }

    this.spinner.show();

    const featureGroup: FeatureGroupModel = {
      description: this.form.get('description').value,
      fgid: this.form.get('fgid').value,
      functionalities: this.form.get('functionalities').value,
      name: this.form.get('name').value
    };

    let service = null;

    if(!!this.data) {
      service = this.featureGroupManagerService.updateFeatureGroup(featureGroup);
    } else {
      service = this.featureGroupManagerService.createFeatureGroup(featureGroup);
    }

    service
      .pipe(
        catchError((error: firebase.FirebaseError) => {
          this.spinner.hide();
          this.errorService.displayError(error);
          return throwError(error);
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(_ => {
        this.form.reset();
        this.initForm();
        this.spinner.hide();
        const mensage = !!this.data
          ? 'Grupo de funcionalidades atualizado com sucesso!'
          : 'Grupo de funcionalidades cadastrado com sucesso!'
        this.snackBar.open(mensage, 'OK');
      });
  }

}
