import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureGroupManagerCreateModalComponent } from './feature-group-manager-create-modal.component';

describe('FeatureGroupManagerCreateModalComponent', () => {
  let component: FeatureGroupManagerCreateModalComponent;
  let fixture: ComponentFixture<FeatureGroupManagerCreateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureGroupManagerCreateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureGroupManagerCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
