import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { FeatureGroupModel } from 'src/app/core/models/feature-group.model';

import { FunctionalityModel } from './../../core/models/functionalities.model';
import { FeatureManagerService } from './../feature-manager/feature-manager.service';
import {
  FeatureGroupManagerCreateModalComponent,
} from './feature-group-manager-create-modal/feature-group-manager-create-modal.component';
import { FeatureGroupManagerService } from './feature-group-manager.service';

@Component({
  selector: 'app-feature-group-manager',
  templateUrl: './feature-group-manager.component.html',
  styleUrls: ['./feature-group-manager.component.scss']
})
export class FeatureGroupManagerComponent implements OnInit, OnDestroy {

  featureGroups = [];
  selectedFeatureGroup: FeatureGroupModel = null;
  functionalityFeatureGroup: FunctionalityModel[] = [];
  functionalityFeatureGroupToAdd: FunctionalityModel[] = [];
  features = [];

  private destroy$ = new Subject();

  constructor(
    private featureGroupManagerService: FeatureGroupManagerService,
    private featureManagerService: FeatureManagerService,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.getAllFeatureGroups()
      .subscribe(_ => {
        this.spinner.hide();
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getAllFeatureGroups(): Observable<any> {
    return this.featureGroupManagerService
      .listAllFeatureGroup()
      .pipe(
        first(),
        tap((featuresGroups: FeatureGroupModel[]) => {
          this.featureGroups = featuresGroups;
        }),
        takeUntil(this.destroy$)  
      );
  }

  getAllFeatures(): Observable<any> {
    return this.featureManagerService
      .listAllFeatures()
      .pipe(
        map((features: FunctionalityModel[]) => {
          if (!!this.selectedFeatureGroup && !!this.selectedFeatureGroup.functionalities) {
            return features.filter((el) => !this.selectedFeatureGroup.functionalities.some((f) => f.fid === el.fid));
          }
          return features;
        }),
        tap(value => {
          this.features = value;
        }),
        takeUntil(this.destroy$)
      );
  }

  onSelectFeatureGroup(): void {
    this.spinner.show();
    this.getAllFeatures()
      .subscribe(_ => {
        this.spinner.hide();
      });
  }

  onFeatureGroupToAddChange($event): void {
    this.functionalityFeatureGroupToAdd = $event;
  }

  onFeatureGroupChange($event): void {
    this.functionalityFeatureGroup = $event;
  }

  private updateFeatureGroup(): Observable<any> {
    return this.featureGroupManagerService
      .updateFeatureGroup(this.selectedFeatureGroup)
      .pipe(
        switchMap(this.getAllFeatures.bind(this)),
        takeUntil(this.destroy$)
      );
  }

  onAddFeature(): void {
    this.spinner.show();
    this.functionalityFeatureGroupToAdd.forEach(item => {
      this.selectedFeatureGroup.functionalities.push(item);
    });
    this.updateFeatureGroup()
      .subscribe(_ => {
        this.spinner.hide();
      });
  }

  onRemoveFeature(): void {
    this.spinner.show();

    this.selectedFeatureGroup.functionalities = this.selectedFeatureGroup.functionalities
      .filter((el) => !this.functionalityFeatureGroup.some((f) => f.fid === el.fid));

    this.updateFeatureGroup()
      .subscribe(_ => {
        this.spinner.hide();
      });
  }

  onOpenModal(): void {
    this.openModal();
  }

  onAbrirModalEditar(): void {
    this.openModal(this.selectedFeatureGroup);
  }

  private cleanScreen(): void {
    this.featureGroups = [];
    this.selectedFeatureGroup = null;
    this.functionalityFeatureGroup = [];
    this.functionalityFeatureGroupToAdd = [];
    this.features = [];
  }

  private openModal(featureGroup?: FeatureGroupModel): void {
    const dialogRef = this.dialog.open(FeatureGroupManagerCreateModalComponent, {
      width: '700px',
      data: featureGroup
    });

    dialogRef
      .afterClosed()
      .pipe(
        tap(_ => {
          this.spinner.show();
          this.cleanScreen();
        }),
        switchMap(this.getAllFeatureGroups.bind(this))
      )
      .subscribe(result => {
          this.spinner.hide();
      });
  }

  private onExcluirFeatureGroup(): void {
    
  }

}
