import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, from } from 'rxjs';

import { FeatureGroupModel } from './../../core/models/feature-group.model';

@Injectable()
export class FeatureGroupManagerService {

  private featureGroups: AngularFirestoreCollection<FeatureGroupModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setFeatureGroupsCollection();
  }

  setFeatureGroupsCollection() {
    this.featureGroups = this.db
      .collection<FeatureGroupModel>('/feature-group');
  }

  listAllFeatureGroup(): Observable<any> {
    return this.featureGroups
      .valueChanges();
  }

  getFeatureGroup(featureGroup: FeatureGroupModel): Observable<any> {
    return this.featureGroups
      .doc<FeatureGroupModel>(featureGroup.fgid)
      .valueChanges();
  }

  updateFeatureGroup(featureGroup: FeatureGroupModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.featureGroups
          .doc(featureGroup.fgid)
          .update(featureGroup)
          .then(
            (userDb: any) => {
              resolve( userDb );
            },
            error => {
              reject( error );
            }
          )
      })
    );
  }

  getNewFeatureGroupId(): string {
    return this.db.createId();
  }

  createFeatureGroup(featureGroup: FeatureGroupModel): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.featureGroups
          .doc<FeatureGroupModel>(featureGroup.fgid)
          .set(featureGroup)
          .then(
            (featureDb: any) => {
              resolve( featureDb );
            },
            error => {
              reject( error );
            }
          );
      })
    );
  }

}
