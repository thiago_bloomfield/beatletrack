import { TestBed } from '@angular/core/testing';

import { FeatureGroupManagerService } from './feature-group-manager.service';

describe('FeatureGroupManagerService', () => {
  let service: FeatureGroupManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatureGroupManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
