import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { FunctionalityModel } from './../../core/models/functionalities.model';

@Injectable()
export class FeatureManagerService {

  private features: AngularFirestoreCollection<FunctionalityModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setFeatureCollection();
  }

  setFeatureCollection() {
    this.features = this.db
      .collection<FunctionalityModel>('/functionalities');
  }

  listAllFeatures(): Observable<FunctionalityModel[]> {
    return this.features.valueChanges();
  }
}
