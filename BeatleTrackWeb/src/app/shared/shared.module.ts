import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ErrorService } from './services/error.service';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';

@NgModule({
  imports: [
    MatSnackBarModule
  ],
  providers: [
    ErrorService
  ],
  declarations: [ConfirmationModalComponent]
})
export class SharedModule { }
