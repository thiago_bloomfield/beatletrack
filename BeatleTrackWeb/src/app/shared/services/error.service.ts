import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ErrorStrategyConfig } from './../config/error-strategy.config';

@Injectable()
export class ErrorService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  displayError(error: firebase.FirebaseError): void {
    const mensage = ErrorStrategyConfig[error.code] || ErrorStrategyConfig['default'];
    this.snackBar.open(mensage, 'OK', {
      duration: 4000,
    });
  }
}
