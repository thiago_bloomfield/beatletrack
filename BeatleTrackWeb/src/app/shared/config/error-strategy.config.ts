export const ErrorStrategyConfig = {
    'auth/user-not-found': 'Usuário não encontrado',
    'auth/wrong-password': 'Senha inválida',
    'default': 'Ocorreu um erro inexperado'
}