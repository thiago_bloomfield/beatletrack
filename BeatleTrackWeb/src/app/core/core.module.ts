import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CustomMaterialModule } from './material-design/custom-material.module';
import { AuthenticationService } from './services/auth/authentication.service';
import { UserService } from './services/user/user.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CustomMaterialModule
  ],
  providers: [
    AuthenticationService,
    UserService
  ],
  exports: [
    CustomMaterialModule,
  ]
})
export class CoreModule { }
