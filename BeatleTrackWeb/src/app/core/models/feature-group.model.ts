import { LanguageValuesModel } from './language-values.model';

export interface FeatureGroupModel {
  description: LanguageValuesModel;
  fgid: string;
  functionalities: Array<any>;
  name: LanguageValuesModel;
}
