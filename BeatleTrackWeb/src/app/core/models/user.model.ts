export interface UserModel {
  displayName: string;
  email: string;
  featureGroup: firebase.firestore.CollectionReference;
  phoneNumber: string;
  photoUrl: string;
  uid: string;
}
