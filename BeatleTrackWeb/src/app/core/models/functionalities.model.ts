import { LanguageValuesModel } from './language-values.model';

export interface FunctionalityModel {
  description: LanguageValuesModel;
  fid: string;
  name: string;
}
