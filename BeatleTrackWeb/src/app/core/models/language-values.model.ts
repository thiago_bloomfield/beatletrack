export interface LanguageValuesModel {
  'pt-br': string;
  en: string;
  es: string;
}
