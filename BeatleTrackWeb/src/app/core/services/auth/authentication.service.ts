import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { auth, User } from 'firebase';
import { from, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  user: User;

  constructor(
    public angularFireAuth: AngularFireAuth,
    public router: Router
  ) {}

  createUserWithEmailAndPassword(email, password) {
    return this.angularFireAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        window.alert("You have been successfully registered!");
        console.log(result.user)
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  signInWithEmailAndPassword(email, password): Observable<any> {
    return from(
      new Promise( (resolve, reject) => {
        this.angularFireAuth
          .signInWithEmailAndPassword(email, password)
          .then(
            (userInfo: auth.UserCredential) => resolve(userInfo),
            error => reject(error)
          ).catch((error) => {
            reject(error);
            throwError(error);
          })
      })
    );
  }

  async register(email: string, password: string) {
    await this.angularFireAuth.createUserWithEmailAndPassword(email, password)
    this.sendEmailVerification();
  }

  async sendEmailVerification() {
    (await this.angularFireAuth.currentUser).sendEmailVerification();
    this.router.navigate(['admin/verify-email']);
  }

  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.angularFireAuth.sendPasswordResetEmail(passwordResetEmail);
  }

  async logout() {
    await this.angularFireAuth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['admin/login']);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }

  async  loginWithGoogle() {
    await this.angularFireAuth.signInWithPopup(new auth.GoogleAuthProvider())
    this.router.navigate(['admin/list']);
  }

}
