import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { FeatureGroupModel } from '../../models/feature-group.model';
import { UserModel } from '../../models/user.model';

@Injectable()
export class UserService {

  private users: AngularFirestoreCollection<UserModel>;

  constructor(
    private db: AngularFirestore
  ) {
    this.setUsersCollection();
  }

  setUsersCollection() {
    this.users = this.db
      .collection<UserModel>('/users');
  }

  searchUserByEmail(user: firebase.User): Observable<any> {
    let userSelected = null;
    return this.users
      .doc(user.email)
      .valueChanges()
      .pipe(
        tap((user: UserModel) => {
          userSelected = user;
        }),
        // switchMap((user: UserModel) => {
        //   console.log(user);
        //   const colectionName = `/${user.featureGroup.path.split('/')[0]}`;
        //   const fgId = user.featureGroup.id;
        //   const user2 = this.db.collection<FeatureGroupModel>(colectionName);
        //   return user2.doc(fgId).valueChanges()
        // }),
        tap(console.log)
      )
  }

}
