import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import { MailClass } from './components/mail/mail.class';

admin.initializeApp();

const mailClass = new MailClass();

export const sendManualEmail = functions.https.onRequest((request, response) => {
  mailClass.sendManualEMail(request, response);
});

export const sendEmail = functions.firestore.document('orders/{orderId}').onCreate((snapshot, context) => {
  mailClass.sendEmail(snapshot, context);
});
