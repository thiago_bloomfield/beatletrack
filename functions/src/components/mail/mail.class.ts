import * as nodemailer from 'nodemailer';

import { environment } from '../../environments/environment';

const _SMTPTransport = nodemailer.createTransport(environment.mail);

export class MailClass {

  constructor() {
    console.log();
  }

  sendManualEMail(request: any, response: any): void {
    const mailOptions = {
      from: `apps.thiagobloomfield@gmail.com`,
      to: 'thiagobloomf@gmail.com',
      subject: 'contact form message',
      html: `<h1>Order Confirmation</h1>
          <p> <b>Email: </b>thiagobloomf@gmail.com</p>`
    };

    _SMTPTransport.sendMail(mailOptions, (error, data) => {
      if (error) {
        console.log('error', error);
        return;
      }
      console.log("Email Enviado!");
      response.send("Email Enviado!");
    });
  }

  sendEmail(snapshot: any, context: any): void {
    console.log("snapshot", snapshot);

    const mailOptions = {
      from: `apps.thiagobloomfield@gmail.com`,
      to: snapshot?.data()?.email,
      subject: 'contact form message',
      html: `<h1>Order Confirmation</h1>
      <p> <b>Email: </b>${snapshot?.data()?.email} </p>`
    };

    _SMTPTransport.sendMail(mailOptions, (error, data) => {
      if (error) {
        console.log(error);
        return;
      }
      console.log("Sent!");
    });
  }
}
